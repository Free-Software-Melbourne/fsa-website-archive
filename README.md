# FSA Website Archive

Archive of Free Software Melbourne /  Free Software Australia Website


## Usage
This archive is primarily for historical and development uses. A live preview can be seen on GitLab Pages:
* https://free-software-melbourne.gitlab.io/fsa-website-archive/mezanine/pages/Free%20Software%20Australia.html


## Installation
Clone the repositry localy and serve using webserver of choice.


## Contributing
If you are interested in contributing to the group get in touch at committee@freesoftware.org.au.


## Authors and acknowledgment
Authors include committe members over the time of the group including:
* Ben Minerds
* ...
* Ben Sturmfels


## License
GPL3+ and CC0.
